<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Users extends CI_Controller{

        public function __construct(){

			parent::__construct();
			$this->load->helper('url_helper');
			$this->load->helper('form_helper');
			$this->load->library('form_validation');
			$this->load->library(array('session'));
            $this->load->model('user_model');
		}

        // Register user
		public function register(){
			
            $data = new stdClass();

            $this->load->helper('form');

			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('password2', 'Confirm Password', 'trim|required|matches[password]');

			if($this->form_validation->run() === false){

				$this->load->view('templates/header');
				$this->load->view('users/register', $data);
				$this->load->view('templates/footer');

			} else {

                $username = $this->input->post('username');
                $email = $this->input->post('email');
                $password = $this->input->post('password');
				// Encrypt password
				if($this->user_model->create_user($username, $email, $password)) {
                    
                    //user creation ok
                    $this->load->view('templates/header');
                    $this->load->view('users/login', $data);
                    $this->load->view('templates/footer');

                } else {
                    
                    //user creation failed. It never happens.
                    $data->error = 'Ha habido un problema creando la cuenta. Inténtalo de nuevo,';

                    $this->load->view('templates/header');
                    $this->load->view('users/register', $data);
                    $this->load->view('templates/footer');
                }
				
			}
		}

		// Log in user
		public function login(){

			$data = new stdClass();

            $this->load->helper('form');

			$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if($this->form_validation->run() == false){

				$this->load->view('templates/header');
				$this->load->view('users/login', $data);
				$this->load->view('templates/footer');

			} else {
				
				// Get username
				$username = $this->input->post('username');
				// Get and encrypt the password
				$password = $this->input->post('password');

                if($this->user_model->resolve_user_login($username, $password)){
				
                    $user_id = $this->user_model->get_user_id_from_username($username);
                    $user = $this->user_model->get_user($user_id);

                    // set session user data
                    $_SESSION['user_id'] = (int)$user->id;
                    $_SESSION['username'] = (string)$user->username;
                    $_SESSION['logged_in'] = (bool)true;
                    $_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
                    $_SESSION['is_admin'] = (bool)$user->is_admin;
                    
                    // user login ok
                    $this->load->view('templates/header');
                    $this->load->view('pages/serviciocolegiado', $data);
                    $this->load->view('templates/footer');

                } else {

                    // login failed
                    $data->error = "Usuario o password erroneo. Inténtalo de nuevo.";

                    // send error to the view
                    $this->load->view('templates/header');
                    $this->load->view('users/login', $data);
                    $this->load->view('templates/footer');
                }
						
			}
		}

		// Log user out
		public function logout() {
		
            // create the data object
            $data = new stdClass();
            
            if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
                
                // remove session datas
                foreach ($_SESSION as $key => $value) {
                    unset($_SESSION[$key]);
                }
                
                // user logout ok
                $this->load->view('templates/header');
                $this->load->view('users/logout/logout_success', $data);
                $this->load->view('templates/footer');
                
            } else {
                
                // there user was not logged in, we cannot logged him out,
                // redirect him to site root
                redirect('/');
                
            }
            
        }

        public function email_validation($username, $hash) {

            // Create the data object
            $data = new stdClass();

            // avoid blank at the end of the url
            $hash = trim($hash);

            if($this->user_model->confirm_account($username, $hash)){

                // account validation ok
                $data->success = 'Enhorabuena. Tu correo se ha confirmado y tu cuenta se ha validado. Por favor, <a href="' . base_url('login') . '">lógate</a>';
                $this->load->view('templates/header');
                $this->load->view('users/register/confirmation', $data);
                $this->load->view('templates/footer');
            } else {

                // account validation failed
                $data->error = 'Ha habido un error. Tu correo no se ha podido validar. Por favor, contacta con el administrador del sitio web.';
                $this->load->view('templates/header');
                $this->load->view('users/register/confirmation');
                $this->load->view('templates/header');

            }
        }

        public function edit($username = false) {

            // a user can only edit his own profile
            // if ($username === false || $username !== $_SESSION['username']) {
            //     redirect(base_url());
            //     return;
            // }

            // create the data object
            $data = new stdClass();

            // load helper
            $this->load->helper('form');

            // form validation
            $password_required_if = $this->input->post('password') ? '|required' : ''; // if there is something on password input, current password is required
            $this->form_validation->set_rules('username', 'Username', 'trim|min_length[4]|max_length[20]|alpha_numeric|is_unique[users.username]', array('is_unique' => 'Este usuario ya existe. Por favor, escoja otro.'));
            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[users.email]', array('is_unique' => 'El correo electrónico introducido ya existe en nuestra base de datos.'));
            $this->form_validation->set_rules('current_password', 'Current Password', 'trim' . $password_required_if . '[callback_verify_current_password]');
            $this->form_validation->set_rules('password', 'New Password', 'trim|min_length[6]|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim|min_length[6]');

            // get the user object
            $user_id = $this->user_model->get_user_id_from_username($username);
            $user    = $this->user_model->get_user($user_id);

            // create breadcrumb
            // $breadcrumb  = '<ol class="breadcrumb">';
            // $breadcrumb .= '<li><a href="' . base_url() . '">Inicio</a></li>';
            // $breadcrumb .= '<li><a href="' . base_url('user/' . $username) . '">' . $username . '</a></li>';
            // $breadcrumb .= '<li class="active">Editar</li>';
            // $breadcrumb .= '</ol>';

            // asign objects to the data object
            $data->user       = $user;
            // $data->breadcrumb = $breadcrumb;

            if ($this->form_validation->run() === FALSE) {

                // validation not ok, send validation errors to the view
                $this->load->view('templates/header');
                $this->load->view('users/profile/edit', $data);
                $this->load->view('templates/footer');

            } else {

                $user_id = $_SESSION['user_id'];
                $update_data = [];

                if ($this->input->post('username') != '') {
                    $update_data['username'] = $this->input->post('username');
                }
                
                if ($this->input->post('email') != '') {
                    $update_data['email'] = $this->input->post('email');
                }
                
                if ($this->input->post('password') != '') {
                    $update_data['password'] = $this->input->post('password');
                }

                // avatar upload
                if (isset($_FILES['usrfile']['name']) && !empty($_FILES['usrfile']['name'])) {

                    //setup upload configuration and load upload library
                    $config['upload_path'] = './uploads/avatars/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = 2048;
                    $config['max_width'] = 1024;
                    $config['max_height'] = 1024;
                    $config['file_ext_tolower'] = true;
                    $config['encrypt_name'] = true;
                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload()) {

                        // upload not OK
                        $error = array('error' => $this->upload->display_errors());
                        $this->load->view('upload_form', $error);
                    } else {

                        // upload ok send name to $update data
                        $update_data['avatar'] = $this->upload->data('file_name');
                    }
                }

                // if everything is ok
                if ($this->user_model->update_user($user_id, $update_data)) {

                    // if username change, update session
                    if(isset($update_data['username'])) {
                        $_SESSION['username'] = $update_data['username'];
                        if ($this->input->post('username') != '') {
                            // a little hook to send success message the new profil edit url if the username was updated
                            $_SESSION['flash'] = 'Tu perfil se ha actualizado con éxito.';
                        }
                    }

                    // fix the fact that a new avatar was not shown until page refresh
                    if(isset($update_data['avatar'])) {
                        $data->user->avatar = $update_data['avatar'];
                    }

                    if ($this->input->post('username') != '') {

                        // redirect to the new profile edit url
                        redirect(base_url('users/' . $update_data['username'] . '/edit'));

                    } else {

                        // create a success message
                        $data->success = 'Tu perfil se ha actualizado con éxtio.';

                        //send success message to the views
                        $this->load->view('templates/header');
                        $this->load->view('users/profile/edit', $data);
                        $this->load->view('templates/footer');
                    }

                } else {

                    // update user not ok: this should never happen
                    $data->error = "Ha habido un problema actualizando tu cuenta. Por favor, inténtalo de nuevo.";

                    // send error to the views
                    $this->load->view('templates/header');
                    $this->load->view('users/profile/edit', $data);
                    $this->load->view('templates/footer');
                }
            }
        }

        public function delete($username = false) {

            // a user can only delete his own profile and must be logged in
            if ($username == false || !isset($_SESSION['username']) || $username !== $_SESSION['username']) {
                redirect(base_url());
                return;
            }

            // create the data object
            $data = new stdClass();

            if ($_SESSION['username'] === $username) {

                // create breadcrumb
                $breadcrumb  = '<ol class="breadcrumb">';
                $breadcrumb .= '<li><a href="' . base_url() . '">Inicio</a></li>';
                $breadcrumb .= '<li><a href="' . base_url('user/' . $username) . '">' . $username . '</a></li>';
                $breadcrumb .= '<li class="active">Eliminar</li>';
                $breadcrumb .= '</ol>';

                $user_id = $this->user_model->get_user_id_from_username($username);
                $data->user = $this->user_model->get_user($user_id);
                $data->breadcrumb = $breadcrumb;

                if ($this->user_model->delete_user($user_id)) {

                    $data->success = 'Tu ususario se ha eliminado con éxito. Hasta pronto.';

                    // user delete ok, load views
                    $this->load->view('templates/header');
                    $this->load->view('users/profile/delete', $data);
                    $this->load->view('templates/footer');

                } else {

                    // a user can only delete his own profile and must be logged in
                    $data->error = 'Ha habido un problema eliminando tu cuenta. Por favor, contacta con el administrador de la web.';

                    // send errors to the views
                    $this->load->view('templates/header');
                    $this->load->view('users/profile/edit', $data);
                    $this->load->view('templates/footer');

                }
            } else {

                // a user can only delete his own profile and must be logged in
                redirect(base_url());
                return;
            }
        }

        public function verify_current_password($str) {

            if ($str != '') {

                if ($this->user_model->resolve_user_login($_SESSION['username'], $str) === true) {

                    return true;
                }

                $this->form_validation->set_message('verify_current_password', 'Tu password no es correcto.');
                return false;
            }

            return true;
        }
    }