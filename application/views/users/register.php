</div>
<img class="img-fluid" src="<?php echo base_url(); ?>assets/images/png/formulario-header.png" style="height:270px; width:100%; margin-bottom: 30px;"></img>
<div class="container">
	<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

	<?php echo form_open('users/register'); ?>
		<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>

			<div class="col-md-4 mx-auto">
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" class="form-control" name="name" placeholder="Nombre">
				</div>
				<div class="form-group">
					<label>Código Postal</label>
					<input type="text" class="form-control" name="zipcode" placeholder="Código Postal">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" name="email" placeholder="Email">
				</div>
				<div class="form-group">
					<label>Usuario</label>
					<input type="text" class="form-control" name="username" placeholder="Usuario">
				</div>
				<div class="form-group">
					<label>Contraseña</label>
					<input type="password" class="form-control" name="password" placeholder="Contraseña">
				</div>
				<div class="form-group">
					<label>Confirmar Contraseña</label>
					<input type="password" class="form-control" name="password2" placeholder="Confirmar Contraseña">
				</div>
				<button type="submit" class="btn btn-primary">Registrarse</button>
			</div>
		</div>
	<?php echo form_close(); ?>
</div>