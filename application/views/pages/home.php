</div>
<div class="presentacion p-5 text-center">
    <h1 class="mb-3 title-main align-middle titulo-presentacion text-uppercase">Bienvenidos</h1>
    <p class="mb-3 text-fff subtitulo-presentacion text-uppercase">A la web del colegio profesional <br> de logopedas de Cantabria</p>
    <a href="<?php echo base_url(); ?>juntagob" class="btn btn-primary btn-white" role="button" style="margin-right:10px; font-size:20px;">CIUDADANOS</a>
    <a href="<?php echo base_url(); ?>juntagob" class="btn btn-primary btn-white" role="button" style="margin-left:10px; font-size:20px;">LOGOPEDAS</a>
</div>


<div class="Primer-Bloque">
    <div class="col-md-6">
        <h1 class="bloque1txt title-dkblue">¿Qué es la logopedia?</h1>
        <br>
        <p class="text-justify bloque1txt text-dkblue">Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi culpa provident eum fugit dolorum placeat vero repudiandae nobis sunt, sit minima tenetur suscipit ipsum itaque! Quis, doloribus. Aspernatur, quod recusandae? Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad quasi suscipit eligendi consectetur vero dolore laboriosam nihil animi, reprehenderit error delectus dolor. Cum eaque soluta sequi saepe deserunt eligendi vel!</p>
        <a href="<?php echo base_url(); ?>faq" class="btn btn-chiki text-uppercase float-right btn-primary" role="button">FAQ</a>
    </div>
    <div class="col-md-6 separadorb1">
        <img class="bloque1img ajusteimginicio2" src="<?php echo base_url(); ?>assets/images/png/Inicio/Recurso 2.png" alt="logos">
    </div>
    <div class="col-md-6">
        <img class="bloque1txt" src="<?php echo base_url(); ?>assets/images/png/Inicio/Recurso 3.png" alt="logos">
    </div>
    <div class="col-md-6">
        <h1 class="bloque1title title-dkblue">Exige logopedas colegiados</h1>
        <br>
        <p class="text-justify bloque1img text-dkblue">Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi culpa provident eum fugit dolorum placeat vero repudiandae nobis sunt, sit minima tenetur suscipit ipsum itaque! Quis, doloribus. Aspernatur, quod recusandae? Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad quasi suscipit eligendi consectetur vero dolore laboriosam nihil animi, reprehenderit error delectus dolor. Cum eaque soluta sequi saepe deserunt eligendi vel!</p>
        <a href="<?php echo base_url(); ?>faq" class="btn btn-chikider text-uppercase float-left btn-primary" role="button">formación</a>
    </div>
</div>

<div class="segundobloque">
    <div class="col-md-7">
        <h1 class="bloque2txt text-white title-white">Nuestro colegio</h1>
        <br>
        <p class="text-justify text-fff bloque2txt">Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi culpa provident eum fugit dolorum placeat vero repudiandae nobis sunt, sit minima tenetur suscipit ipsum itaque! Quis, doloribus. Aspernatur, quod recusandae? Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad quasi suscipit eligendi consectetur vero dolore laboriosam nihil animi, reprehenderit error delectus dolor. Cum eaque soluta sequi saepe deserunt eligendi vel!</p>
        <a href="<?php echo base_url(); ?>juntagob" class="btn bloque1btn text-uppercase float-right btn-white btn-primary" role="button">Junta</a>
    </div>
</div>

<div class="tercerbloque">
    <div class="col-md-6">
        <img class="bloque1txt ajusteimginicio" src="<?php echo base_url(); ?>assets/images/png/Inicio/Recurso 10.png" alt="logos">
    </div>
    <div class="col-md-6">
        <h1 class="bloque1img title-dkblue">¿Por qué colegiarse?</h1>
        <br>
        <p class="text-justify bloque1img text-dkblue">Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi culpa provident eum fugit dolorum placeat vero repudiandae nobis sunt, sit minima tenetur suscipit ipsum itaque! Quis, doloribus. Aspernatur, quod recusandae? Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad quasi suscipit eligendi consectetur vero dolore laboriosam nihil animi, reprehenderit error delectus dolor. Cum eaque soluta sequi saepe deserunt eligendi vel!</p>
        <a href="<?php echo base_url(); ?>users/register" class="btn bloque1btn text-uppercase float-left btn-primary" role="button">colegiarse</a>
    </div>
</div>

<div class="cuartobloque">
    <div class="col-md-7">
        <h1 class="bloque2txt text-white title-white">¿Quieres colegiarte?</h1>
        <br>
        <p class="text-justify text-fff bloque2txt">Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi culpa provident eum fugit dolorum placeat vero repudiandae nobis sunt, sit minima tenetur suscipit ipsum itaque! Quis, doloribus. Aspernatur, quod recusandae? Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad quasi suscipit eligendi consectetur vero dolore laboriosam nihil animi, reprehenderit error delectus dolor. Cum eaque soluta sequi saepe deserunt eligendi vel!</p>
        <a href="<?php echo base_url(); ?>cplc" class="btn bloque1btn text-uppercase float-right btn-white btn-primary" role="button">formación</a>
    </div>
</div>
<div class="quintobloque">
    <div class="margenes">
        <h1 class="text-center title-dkblue">¿Eres logopeda?</h1>
        <br>
        <p class="text-center text-dkblue">Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi culpa provident eum fugit dolorum placeat vero repudiandae nobis sunt, sit minima tenetur suscipit ipsum itaque! Quis, doloribus. Aspernatur, quod recusandae? Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad quasi suscipit eligendi consectetur vero dolore laboriosam nihil animi, reprehenderit error delectus dolor. Cum eaque soluta sequi saepe deserunt eligendi vel!</p>
        <div class="text-center">
        <a href="<?php echo base_url(); ?>codigo" class="btn bloque5btn1 text-uppercase btn-primary" role="button">código</a>
        <a href="<?php echo base_url(); ?>estatutos" class="btn bloque5btn2 text-uppercase btn-primary" role="button">estatutos</a>
        </div>
    </div>
</div>
<div class="sextobloque">
    <h1 class="titulo6 text-white title-white">Últimas Noticias</h1>
    <div class="carrusel-colaboradores text-center">
    <ul class="cards align-items-center">
        <li>
            <a href="" class="card">
            <img src="<?php echo base_url(); ?>assets/images/png/545x270__0004_CPLC_WEB_JUNTA.jpg" class="card__image" alt="" />
            <div class="card__overlay">
                <div class="card__header">
                <svg class="card__arc" xmlns="http://www.w3.org/2000/svg"><path /></svg>                     
                <img class="card__thumb" src="<?php echo base_url(); ?>assets/images/empresas/LOGOPEDAS_WEB_EMPRESAS_SANTANDER.png" alt="" />
                <div class="card__header-text">
                    <h3 class="card__title">Jessica Parker</h3>            
                    <span class="card__status">1 hour ago</span>
                </div>
                </div>
                <p class="card__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores, blanditiis?</p>
            </div>
            </a>      
        </li>
        <li>
            <a href="" class="card">
            <img src="<?php echo base_url(); ?>assets/images/png/545x270__0004_CPLC_WEB_JUNTA.jpg" class="card__image" alt="" />
            <div class="card__overlay">        
                <div class="card__header">
                <svg class="card__arc" xmlns="http://www.w3.org/2000/svg"><path /></svg>                 
                <img class="card__thumb" src="<?php echo base_url(); ?>assets/images/empresas/LOGOPEDAS_WEB_EMPRESAS_SANTANDER.png" alt="" />
                <div class="card__header-text">
                    <h3 class="card__title">kim Cattrall</h3>
                    <span class="card__status">3 hours ago</span>
                </div>
                </div>
                <p class="card__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores, blanditiis?</p>
            </div>
            </a>
        </li>
        <li>
            <a href="" class="card">
            <img src="<?php echo base_url(); ?>assets/images/png/545x270__0004_CPLC_WEB_JUNTA.jpg" class="card__image" alt="" />
            <div class="card__overlay">
                <div class="card__header">
                <svg class="card__arc" xmlns="http://www.w3.org/2000/svg"><path /></svg>                 
                <img class="card__thumb" src="<?php echo base_url(); ?>assets/images/empresas/LOGOPEDAS_WEB_EMPRESAS_SANTANDER.png" alt="" />
                <div class="card__header-text">
                    <h3 class="card__title">kim Cattrall</h3>
                    <span class="card__status">3 hours ago</span>
                </div>          
                </div>
                <p class="card__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores, blanditiis?</p>
            </div>
            </a>
        </li>    
        </ul>
    </div>
</div>
<div class="septimobloque text-center">
    <hr class="rounded">
    <h1 class="text-lgtblue">Acuerdos con empresas</h1>
    <a href="https://www.doctorshop.es" target="_blank"><img src="<?php echo base_url(); ?>assets/images/empresas/LOGOPEDAS_WEB_EMPRESAS_DOCTORSHOP.png" class="acuerdoempresainicio grayscale"></img></a>
    <a href="https://www.happycar.es" target="_blank"><img src="<?php echo base_url(); ?>assets/images/empresas/LOGOPEDAS_WEB_EMPRESAS_HAPPYCAR.png" class="acuerdoempresainicio grayscale"></img></a>
    <a href="https://www.facebook.com/santanderjavierjoyeros" target="_blank"><img src="<?php echo base_url(); ?>assets/images/empresas/LOGOPEDAS_WEB_EMPRESAS_JAVIER.png" class="acuerdoempresainicio grayscale"></img></a>
    <a href="https://logopedicum.com" target="_blank"><img src="<?php echo base_url(); ?>assets/images/empresas/LOGOPEDAS_WEB_EMPRESAS_LOGOPEDICUM.png" class="acuerdoempresainicio grayscale"></img></a>
    <a href="https://www.ondaeduca.com" target="_blank"><img src="<?php echo base_url(); ?>assets/images/empresas/LOGOPEDAS_WEB_EMPRESAS_ONDAEDUCA.png" class="acuerdoempresainicio grayscale"></img></a>
</div>
<div class="octavobloque">
<script type="text/javascript" src="https://form.jotform.com/jsform/212343600643041"></script>
    <div class="col-md-6">
        <div class="bloque1txt text-white">
            <h1 style="margin-left:25px; margin-bottom:20px;">Nuestros Datos</h1> 
            <div class="row contactinicio">
                <a href="https://www.google.com/maps?ll=43.459844,-3.807338&z=18&t=m&hl=es&gl=ES&mapclient=embed&cid=5877081261154725323" target="_blank" class="col-md-1"><i class="fa fa-globe" style="font-size:36px"></i></a>
                <a target="_blank" class="contactext text-justify" href="https://www.google.com/maps?ll=43.459844,-3.807338&z=18&t=m&hl=es&gl=ES&mapclient=embed&cid=5877081261154725323">Calle Calderón de la Barca, 15,<br> 39002 Santander, Cantabria</a>
            </div>
            <br>
            <div class="row contactinicio">
                <a target="_blank" class="col-md-1" href="tel:+34942052099"><i class="fa fa-phone-square" style="font-size:36px"></i></a>
                <a target="_blank" class="contactext text-justify" href="tel:+34942052099">942 052 099</a>
            </div>
            <br>
            <div class="row contactinicio">
                <a target="_blank" href="mailto:colegio@logopedascantabria.org" class="col-md-1"><i class="fa fa-envelope-square" style="font-size:36px;"></i></a>
                <a target="_blank" href="mailto:colegio@logopedascantabria.org" class="contactext text-justify">colegio@logopedascantabria.org</a>
            </div>
        </div>
    </div>

    <div class="col-md-6 bloque1img text-white">
        <h1 style="margin-left:-10px; margin-top: 20px;">Nuestro horario</h1>
            <div class="row">
            <p class="horario">Lunes y Jueves</p><p class="horario2 text-right">17:00 a 20:00</p>
            </div>
            <div class="row">
                <p class="horario">Martes y Miércoles</p><p class="horario2 text-right">10:00 a 13:00</p>
            </div>
            <div class="row">
                <p class="horario">Viernes, sábado y domingo</p><p class="horario2 text-right">Cerrado</p>
            </div>
        </div>
</div>
