</div>
<div id="cabecera">
    <img src="<?php echo base_url(); ?>assets/images/png/junta.png" style="height: 270; width:100%;"></img>
</div>
<div class="container text-justify">
        <div class="white minusmargin2">
            <h3 class="section-subheading text-muted">COMPETENCIAS DE LA JUNTA DE GOBIERNO:</h3>
            <ul class="ml-4 ajuste">
            <li>Cumplir y hacer cumplir los acuerdos de la Asamblea o Junta General, los Estatutos y la legislación vigente que afecte al Colegio.</li>
            <li>Decidir sobre las solicitudes de colegiación.</li>
            <li>Proponer la cuantía de las cuotas ordinarias y extraordinarias y formular balances, cuentas, inventario, memoria anual y planes de actuación para someterlos a la Asamblea o Junta.</li>
            <li>Defender los intereses profesionales de los colegiados, ostentado en su ámbito la plena representación del Colegio, sin perjuicio de la delegación de todas o partes de sus facultades.</li>
            <li>Acordar y aplicar, de conformidad con estos Estatutos y el reglamento disciplinario, las sanciones que procedan.</li>
            <li>Ejercer cuantas funciones correspondan al Colegio, siempre que no estén expresamente reservadas a la Asamblea o Junta General, desarrollando, en su caso, las líneas generales que ésta señale.</li>
            <li>Imponer, previa instrucción del expediente oportuno, las sanciones disciplinarias que correspondan.</li>
            <li>Crear o reestructurar las comisiones o grupos de trabajo necesarias para el mejor cumplimiento de las funciones colegiales.</li>
            <li>Fijar la fecha de celebración de las Asamblea o Juntas Ordinarias y Extraordinarias.</li>
            </ul>
            <br>
            <h3 class="section-subheading text-muted">Esta es nuestra junta de gobierno:</h3>
        </div>
    <div class="row blue">
        <div class="text-white">
            <img class="col-md-5 decana cromo img-fluid" src="<?php echo base_url(); ?>assets/images/png/prueba1.jpeg">
            <div class="col-md-6 txt">
                <p class="font-italic"><u>Facultades y atribuciones</u></p>
                <p>1.- Ejercer, en virtud de su cargo y sin perjuicio de la representación colectiva del Comité Ejecutivo o Junta de Gobierno del Colegio, la representación del Colegio.</p>
                <p>2.- Asumir la alta dirección del Colegio y de los servicios colegiales en cuantos asuntos lo requieran, de acuerdo con las normas de la Asamblea o Junta General y el Comité 
                <p>3.- Ejecutivo o Junta de Gobierno del Colegio.</p>
                <p>4.- Convocar la Comisión Permanente y el Comité Ejecutivo o Junta de Gobierno del Colegio.</p>
                <p>5.- Presidir las reuniones que celebren los Órganos de Gobierno del Colegio y Comisiones del mismo.</p>
                <p>6.- Contar, tanto en el Comité Ejecutivo o Junta de Gobierno del Colegio como en la Comisión Permanente, en su caso, con voto dirimente si se produjera empate en las votaciones de los miembros asistentes en la respectiva reunión.</p>
                <p>7.- Firmar o autorizar con su visto bueno, según proceda, las actas, certificaciones, informes, circulares y normas generales.</li>
                <p>8.- Ordenar los pagos que hayan de realizarse con cargo a los fondos del Colegio y autorizar la disposición de los fondos de las cuentas o depósitos, uniendo su firma a la del Tesorero.</p>
                <p>9.- Delegar en la Vicepresidenta cometidos concretos.</p>
            </div>
        </div>
        <div class="titulocromo">
            <h4 class="font-weight-bold">DECANA</h4>
            <p>Laura Fernández - Victorio Alonso</p>
        </div>
    </div>
    <div class="row white">
        <img class="col-md-5 decana cromo" src="<?php echo base_url(); ?>assets/images/png/prueba1.jpeg">
        <div class="col-md-6 txt text-black">
            <p class="font-italic"><u>Competencias</u></p>
            <p>Sustituir a la Presidenta en caso de ausencia, enfermedad o vacante, con las mismas facultades y atribuciones, así como llevar a cabo todas aquellas funciones colegiales que le encomiende o delegue la Presidenta.</p>
        </div>
        <div class="titulocromob">
            <h4 class="font-weight-bold">VICEDECANA</h4>
            <p>Milagros Matarazzo Zinoni</p>
        </div>
    </div>

    <div class="row blue">
        <img class="col-md-5 decana cromo" src="<?php echo base_url(); ?>assets/images/png/prueba1.jpeg">
        <div class="text-white col-md-6 txt">
            <p class="font-italic"><u>Funciones</u></p>
            <p>1.- Asistir a las reuniones con voz y voto.</p>
            <p>2.- Efectuar la convocatoria de las sesiones de los órganos colegiados por orden de su Presidente o Decano, así como las citaciones a los miembros del mismo.</p>
            <p>3.- Recibir los actos de comunicación de los miembros del órgano colegiado y, por tanto, las notificaciones, peticiones de datos, rectificaciones o cualquiera otra clase de escritos de los que deba tener conocimiento.</p>
            <p>4.- Preparar el despacho de los asuntos, redactar y autorizar las actas de las sesiones.</p>
            <p>5.- Expedir certificaciones de las consultas, dictámenes y acuerdos aprobados.</p>
            <p>6.- Redactar las actas y correspondencia oficial, dirigiendo los trabajos administrativos del Colegio, así como el archivo y custodia de la documentación.</p>
            <p>7.- Llevar el Registro de Logopedas sanitarios y el Registro de Sociedades Profesionales.</p>
            <p>8.- Desempeñar la jefatura directa e inmediata de los servicios colegiales y de las personas afectas a los mismos.</p>
            <p>9.- Cuantas otras funciones sean inherentes a su condición de Secretario.</p>
            
            <p>La decisión sobre la sustitución del Secretario en supuestos de vacante, ausencia o enfermedad se realizará por el Comité Ejecutivo o Junta de Gobierno del Colegio.</p>
        </div>
        <div class="titulocromo">
            <h4 class="font-weight-bold">SECRETARIA</h4>
            <p>Marta Teresa Candela de Aroca</p>
        </div>
    </div>

    <div class="row white">
        <img class="col-md-5 decana cromo" src="<?php echo base_url(); ?>assets/images/png/prueba1.jpeg">
        <div class="col-md-6 txt">
            <p class="font-italic"><u>Competencias</u></p>
            <p>Custodia y responsabilidad de los fondos de la Corporación, la ejecución o efectividad de los cobros y pagos, llevando al efecto el oportuno libro de Caja y la autorización, junto con la firma de la Presidenta o Decana, de las disposiciones de fondos.</p>
            <p>Corresponderá al Tesorero, la intervención de todos los documentos contables, así como la redacción, para su examen y formulación por el Comité Ejecutivo o Junta de Gobierno del Colegio, de los balances, cuentas, presupuestos y de cualquier estudio económico que se le encargue por la Comisión Permanente o El Comité Ejecutivo o Junta de Gobierno del Colegio.</p>
            <p>En caso de vacante, ausencia o enfermedad del tesorero, su firma podrá ser sustituida por la de quien o quienes reglamentariamente se determinen.</p>
        </div>
        <div class="titulocromob">
            <h4 class="font-weight-bold">TESORERA</h4>
            <p>Bianka González Crespo</p>
        </div>
    </div>

    <div class="row blue">
        <img class="col-md-4 decana cromo" src="<?php echo base_url(); ?>assets/images/png/prueba1.jpeg">
        <div class="col-md-3 mt">
            <p class="minusmargin text-white text-left">Llevar a cabo todas aquellas funciones colegiales que les encomiende la Presidenta.</p>
        </div>
        <img class="col-md-4 decana floatRight" src="<?php echo base_url(); ?>assets/images/png/prueba1.jpeg">
        <div class="titulocromo">
            <h4 class="font-weight-bold">VOCAL 1ª</h4>
            <p>Marina Terán Cruz y Carmen Gainza Brandariz</p>
        </div>
    </div>
</div>
   