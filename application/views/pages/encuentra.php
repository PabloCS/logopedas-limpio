</div>
<img src="<?php echo base_url(); ?>assets/images/png/encuentra-header.png" style="height: 270; width:100%;"></img>
<div class="container text-center">
    <h3><strong>Encuentra aquí tu Logopeda</strong></h3>
    <br>
    <br>
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "logopedas";

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT Colegiado, Nombre, Apellidos, LocalidadTrabajo, Telefono FROM colegiados2";
        $result = $conn->query($sql);

        if (!empty($result) && $result->num_rows > 0) {
        echo "<table id='mitabla'class='table table-striped table-bordered' style='width:100%'><thead><tr><th>Colegiado</th><th>Nombre</th><th>Apellidos</th><th>Localidad</th></tr></thead>";
        // output data of each row
        while($row = $result->fetch_assoc()) {
            echo "<tr><td>".$row["Colegiado"]."</td><td>".$row["Nombre"]."</td><td>".$row["Apellidos"]."</td><td>".$row["LocalidadTrabajo"]."</td></tr>";
        }
        echo "</table>";
        } else {
        echo "0 results";
        }
        $conn->close();
    ?>
</div>