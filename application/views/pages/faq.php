</div>
<img src="<?php echo base_url(); ?>assets/images/png/faq-header.png" style="height: 270; width:100%;"></img>
<div class="container">
  <div class="faq-content">
    <div class="faq-question">
      <input id="q1" type="checkbox" class="panel">
      <div class="plus">+</div>
      <label for="q1" class="panel-title">¿Qué es la logopedia?</label>
      <div class="panel-content">La logopedia es una profesión sanitaria, cuyo campo de actuación es la prevención, detección, evaluación, diagnóstico y tratamiento de los trastornos de la comunicación, del lenguaje, del habla, de la voz, de la audición y de las funciones orales asociadas (respiración, succión, masticación y deglución).</div>
    </div>
    
    <div class="faq-question">
      <input id="q2" type="checkbox" class="panel">
      <div class="plus">+</div>
      <label for="q2" class="panel-title">¿Por qué exigir un logopeda colegiado?</label>
      <div class="panel-content">
          <ul>
          <li>El logopeda colegiado significa certificado oficial de su formación específica en logopedia y es garantía de una buena práctica profesional.</li>
          <li>El logopeda colegiado está obligado a seguir fielmente el Código Deontológico del Consejo General de Colegios de Logopedas de España (CGCL), lo cual revierte directamente en la defensa de los derechos de las personas usuarias de los servicios de logopedia debido al alto nivel de ética profesional exigida por el CPLC.</li>
          <li>El 7 de octubre de 2004, el Parlamento de Cantabria aprueba por unanimidad la creación del Colegio Profesional de Logopedas en Cantabria. La ley de Cantabria 3/2004 obliga a la colegiación a todos aquellos profesionales que ejerzan la logopedia, excepto en el ámbito público, con el objetivo de regular, desde el CPLC, las actividades de los logopedas de nuestra región.</li>
          </ul>
      </div>
    </div>
    
    <div class="faq-question">
      <input id="q3" type="checkbox" class="panel">
      <div class="plus">+</div>
      <label for="q3" class="panel-title">¿Dónde trabajan los logopedas?</label>
      <div class="panel-content">
          <ul>
              <li>Su campo de actuación es amplio: puede abarcar desde la atención temprana en la población infantil, hasta la intervención en la edad adolescente, adulta y en la tercera edad.</li>
              <li>Los profesionales logopedas pueden intervenir en diferentes ámbitos de actuación como en el sanitario,  en los socio-asistenciales, en el educativo y en consulta privada.</li>
          </ul>
      </div>
    </div>

    <div class="faq-question">
      <input id="q4" type="checkbox" class="panel">
      <div class="plus">+</div>
      <label for="q4" class="panel-title">¿Qué alteraciones tratan los logopedas?</label>
      <div class="panel-content">
          <ul>
              <li><strong>Área del habla</strong>: discurso de la producción de sonido, articulación, apraxia del habla, disartria, resonancia, trastornos de la fluidez del habla (hiperfluidez, tartamudeo, etc.), comunicación prelingüística y paralingüística, etc.</li>
              <li><strong>Área de la voz</strong>: la fonación de calidad, campo vocal, volumen, respiración, trastornos de la resonancia, disfonías, afonías, etc.</li>
              <li><strong>Área del lenguaje</strong> (comprensión y expresión): fonética, fonología, morfo-sintaxis, semántica, la pragmática o uso del lenguaje, Trastorno Específico del Lenguaje (TEL), deficiencias, autismo, alteraciones en el aprendizaje lectoescritor (dislexias, disgrafías, discalculias, disortografía…) y trastornos del lenguaje por daño cerebral (afasias, entre otras).</li>
              <li><strong>Área de la cognición</strong>: atención, memoria, concentración, secuenciación, resolución de problemas y capacidades ejecutivas. También se incluyen los trastornos asociados a procesos degenerativos: deterioro en la comunicación por envejecimiento, trastornos neurodegenerativos infecciosos, demencias, etc.</li>
              <li><strong>Área de la alimentación</strong>: masticación, deglución, succión, fase oral, fase faríngea, fase esofágica, miología orofacial por vía oro-motora, etc.</li>
              <li><strong>Área de la audición</strong>: dificultades de lenguaje, habla y voz debidas a pérdidas auditivas de transmisión, neurosensoriales y mixtas, hipoacusias, presbiacusia, síndromes adversos, etc. </li>
          </ul>
      </div>
    </div>

    <div class="faq-question">
      <input id="q5" type="checkbox" class="panel">
      <div class="plus">+</div>
      <label for="q5" class="panel-title">¿Quieres participar en un voluntariado profesional?</label>
      <div class="panel-content">
          <p>La Asociación Felicidad sin Fronteras es una organización no gubernamental para el desarrollo sin ánimo de lucro. Nace a raíz de la inquietud de un grupo de jóvenes voluntarios de diferentes países que intentan movilizar a la sociedad por una cuestión de justicia y solidaridad. Fue creada en 2010, y desde entonces lleva realizando numerosos proyectos con diferentes colectivos, sobretodo niños y personas con discapacidad.</p>
          <p>Éstos se realizan mayormente en Azrou, una ciudad bereber situada en Marruecos, a 89 km al sur de Fez, en la región de Meknes-Talifait., ubicada en un territorio con un insólito encanto por su riqueza en biodiversidad faunística y florística.</p>
          <p>Cuando visitas el país y te adentras en la cultura, te das cuenta de las necesidades y pocos recursos con los que cuentan; en Azrou se creó un centro de salud y rehabilitación con suficientes materiales para la práctica, pero la inexistencia de profesionales hace que los niños y las familias no reciban tratamiento, Es por ello que tras uno de los proyectos del verano de 2015, un grupo de voluntarios nos preguntamos “¿Por qué no hacer un proyecto de larga duracción, dónde no sólo ofrezcamos tratamiento multidisciplinar a los niños con discapacidad y ayuda a las familias, si no que podamos intercambiar formación con personal local para una futura autogestión del centro? Es por ellos que empezamos a crear este maravilloso proyecto; Evaluación, rehabilitación e integración de niños con discapacidad.</p>
          <p>El voluntariado está abierto a profesionales del ámbito de la salud y sociosanitario (TO, fisioterapeutas, logopedas, psicólogos, médicos...). Se realizará entre Enero y diciembre de 2016, los voluntarios podrán inscribirse en periodos mínimos de 3 semanas y máximos a elección, habiendo un límite de 10 voluntarios por periodo. El proyecto tendrá un coste de 80 euros y éste incluye; alojamiento, 3 comidas diarias, material necesario, recogida y vuelta al aeropuerto y clases de árabe. No incluye avión y gastos propios.</p>
          <p>Además durante vuestra estancia realizaremos diferentes excursiones por la zona (Desierto, Bosque de Cedros, ciuda de Meknes...)</p>
          <p >Las inscripciones ya están abiertas, para más información y cualquier duda al respecto podéis visitar nuestra página <a href="https://felicidadsfronteras.org/" target="_blank" class="stretched-link">www.felicidadsfronteras.org</a>, escribirnos al mail: <a href="mailto: informatefsf@gmail.com">informatefsf@gmail.com</a> o al número de teléfono (vía Whatsapp) <a href='https://wa.me/+34610963104' target='_blank'>+34 610963104</a> (Cristina Morgades) o <a href='https://wa.me/+34660676876' target='_blank'>+34 660676876</a> (Eva Montero)</p>
      </div>
    </div>
  </div>
          </div>