<div class="container">
    <div class="row mt-5">
        <div class="texto col-lg-6 col-md-12">
            <h4 style="font-size:3rem; font-weight: 600;">Formación</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur beatae porro molestias libero dolore cum, itaque illum dolor asperiores nulla sapiente eveniet sequi doloribus commodi veritatis quibusdam nostrum nobis reiciendis excepturi et, molestiae quidem amet. Veritatis velit repellat ad ab quibusdam quia molestiae autem praesentium pariatur?</p>
        </div>
        <a class="col-lg-6 col-md-12 align-center" href="<?php echo base_url(); ?>cplc">
            <img class="img-fluid" src="<?php echo base_url(); ?>assets/images/png/cursos-cplc-img.png"></img>
        </a>
    </div>
    <div class="row mt-5">
        <a class="col-lg-6 col-md-12 align-center" href="<?php echo base_url(); ?>otroscursos">
            <img class="img-fluid" src="<?php echo base_url(); ?>assets/images/png/otroscursos-img.png"></img>
        </a>
        <a class="col-lg-6 col-md-12" href="<?php echo base_url(); ?>masteryp">
            <img class="img-fluid" src="<?php echo base_url(); ?>assets/images/png/master-img.png"></img>
        </a>
    </div>
</div>