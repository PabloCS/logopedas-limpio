<div class="row">
		<div class="col-md-12">
			<?= $breadcrumb ?>
		</div>
		<div class="col-md-12">
			<div class="page-header">
				<h1>Crear nuevo foro</h1>
			</div>
		</div>
		<?php if ($login_as_admin_needed) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<p>Necesitas ser administrador para crear un nuevo foro</p>
					<p>Por favor, <a href="<?= base_url('login') ?>">inicia sesión.</a>.</p>
				</div>
			</div>
		<?php else : ?>
			<?php if (validation_errors()) : ?>
				<div class="col-md-12">
					<div class="alert alert-danger" role="alert">
						<?= validation_errors() ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if (isset($error)) : ?>
				<div class="col-md-12">
					<div class="alert alert-danger" role="alert">
						<?= $error ?>
					</div>
				</div>
			<?php endif; ?>
			<div class="col-md-12">
				<?= form_open() ?>
					<div class="form-group">
						<label for="title">Título</label>
						<input type="text" class="form-control" id="title" name="title" placeholder="Introduce el nombre de tu foro" value="<?= $title ?>">
					</div>
					<div class="form-group">
						<label for="description">Descripción </label>
						<textarea rows="6" class="form-control" id="description" name="description" placeholder="Introduce una breve descripción sobre el foro(80 caractéres máximo)"><?= $description ?></textarea>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-info" value="Crear foro">
					</div>
				</form>
			</div>
		<?php endif; ?>
	</div><!-- .row -->