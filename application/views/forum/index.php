<div class="row white">
	<div class="col-md-12">
			<table class="table table-striped table-condensed table-hover">
				<caption></caption>
				<thead>
					<tr>
						<th>Foros</th>
						<th>Temas</th>
						<th>Entradas</th>
						<th class="hidden-xs">Últimos temas</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($forums) : ?>
						<?php foreach ($forums as $forum) : ?>
							<tr>
								<td>
									<p>
										<a class="test" href="<?= base_url('forum/index/'.$forum->slug) ?>"><?= $forum->title ?></a><br>
										<small><?= $forum->description ?></small>
									</p>
								</td>
								<td>
									<p>
										<small><?= $forum->count_topics ?></small>
									</p>
								</td>
								<td>
									<p>
										<small><?= $forum->count_posts ?></small>
									</p>
								</td>
								<td class="hidden-xs">
									<?php if ($forum->latest_topic->title !== null) : ?>
										<p>
											<small><a href="<?= base_url('topic/' . $forum->latest_topic->permalink) ?>"><?= $forum->latest_topic->title ?></a><br>por <a href="<?= base_url('users/' . $forum->latest_topic->author . '/edit') ?>"><?= $forum->latest_topic->author ?></a>, <?= $forum->latest_topic->created_at ?></small>
										</p>
									<?php else : ?>
										<p>
											<small>sin temas</small>
										</p>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
			
		</div>
		
		<?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] === true) : ?>
			<div class="col-md-12">
				<a href="<?= base_url('forum/create_forum') ?>" class="btn btn-info my-2 my-sm-0">Crear un nuevo foro</a>
			</div>
		<?php endif; ?>
		
</div>
		