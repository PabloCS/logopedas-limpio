<div class="row">
		<!-- <div class="col-md-12">
			<?= $breadcrumb ?>
		</div> -->
		<div class="col-md-12">
			<div class="page-header">
			<?php if (isset($forum) && !empty($forum)) : ?>
				<h1 class="pb-5"><?php echo($forum->title); ?></h1>
				<p><?php echo($forum->description); ?></p>
				<?php endif; ?>
			</div>
		</div>
		
		<div class="col-md-12">
			<?php if (isset($topics) && !empty($topics)) : ?>
				<table class="table table-striped table-condensed table-hover">
					<caption></caption>
					<thead>
						<tr>
							<th>Temas</th>
							<th>Entradas</th>
							<th class="hidden-xs">Últimas entradas</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($topics as $topic) : ?>
							<tr>
								<td>
									<p>
										<a href="<?= base_url('topic/' . $topic->permalink) ?>"><?= $topic->title ?></a><br>
										<small>creado por <a href="<?= base_url('users/' . $topic->author . '/edit') ?>"><?= $topic->author ?></a>, <?= $topic->created_at ?></small>
									</p>
								</td>
								<td>
									<p>
										<small><?= $topic->count_posts ?></small>
									</p>
								</td>
								<td class="hidden-xs">
									<p>
										<small>por <a href="<?= base_url('users/' . $topic->latest_post->author . '/edit') ?>"><?= $topic->latest_post->author ?></a><br><?= $topic->latest_post->created_at ?></small></p>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php else : ?>
				<h4>Aún sin temas...</h4>
			<?php endif; ?>
		</div>
		
		<?php if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true) : ?>
			<div class="col-md-12 pt-5 pb-5">			
				<a href="<?= base_url().$forum->slug . '/create_topic' ?>" class="btn btn-primary">Crear un nuevo tema</a>
			</div>
		<?php endif; ?>
		
	</div><!-- .row -->