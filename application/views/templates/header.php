<html>
    <head>
        <title>Logopedas</title>
        <link rel="stylesheet" href="https://bootswatch.com/4/united/bootstrap.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <link rel="icon" href="<?=base_url()?>assets/images/png/favicon.ico" type="image/vnd.microsfot.icon">
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
        <script src="https://js.stripe.com/v3/"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 

    </head>
    <body id="page-top">
        <!-- Navigation-->
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="prueba collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
                        <!-- LOGOTIPO DEL NAVEGADOR -->
                        <a class="alineadomenu navbar-brand mt-2 mt-lg-0" href="<?php echo base_url(); ?>"><img class="logo_header" src="/logopedas/assets/images/png/logo_header.png"></img></a>

                        <!-- MENÚ -->
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="<?php echo base_url(); ?>colegio" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">EL COLEGIO</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>juntagob">JUNTA DE GOBIERNO</a>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>estatutos">ESTATUTOS</a>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>codigo">CÓDIGO DEONTOLÓGICO</a>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>acuerdos">ACUERDOS CON EMPRESAS</a>
                                    </div>
                            </li>
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="<?php echo base_url(); ?>logopedas" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">CIUDADANOS</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>encuentra">BUSCA TU LOGOPEDA</a>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>faq">PREGUNTAS FRECUENTES</a>
                                    </div>
                            </li>
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="<?php echo base_url(); ?>formacion" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">FORMACIÓN</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>cplc">CURSOS CPLC</a>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>otroscursos">OTROS CURSOS</a>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>masteryp">MÁSTER Y POSTGRADO</a>
                                    </div>
                            </li>
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="<?php echo base_url(); ?>logopedas" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NOTICIAS</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>forum/index">FORO</a>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>videonoticias">VIDEONOTICIAS</a>
                                    </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>contacto">CONTACTO</a>
                            </li>
                        </ul>
                        <div class="d-flex align-items-center mt-3 justify-content-end">
                            <?php if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true) : ?>
                                <a href="<?php echo base_url(); ?>serviciocolegiado" class="alineadobotonmenu2 btn btn-info px-3 m2-2" type="submit"">SERVICIOS LOGOPEDAS</a>
                                <a href="<?= base_url('users/logout/logout_success') ?>" class="alineadobotonmenu btn btn-info px-3 me-2" type="submit">SALIR</a>
                            <?php else : ?>
                                <a href="<?php echo base_url(); ?>users/register" class="alineadobotonmenu2 btn btn-info px-3 me-2" type="submit">COLÉGIATE</a>
                                <a href="<?php echo base_url(); ?>users/login" class="alineadobotonmenu btn btn-info px-3 me-2" type="submit">ACCESO A COLEGIADOS</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        <div class="container">
           