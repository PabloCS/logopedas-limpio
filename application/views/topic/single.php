<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<!-- <div class="col-md-12">
			<?= $breadcrumb ?>
		</div> -->
		<div class="col-md-12">
			<div class="page-header">
				<h1><?= $topic->title ?></h1>
				<p><?= $topic->content ?></p>
			</div>
		</div>
		
		<?php foreach ($posts as $post) : ?>
			<div class="col-md-12">
				<article class="panel panel-default">
					<div class="panel-body">
						<header class="post-header">
							<small><a href="<?= base_url('users/' . $post->author . '/edit') ?>"><?= $post->author ?></a>, <?= $post->created_at ?></small>
						</header>
						<div class="post-content">
							<?= $post->content ?>
						</div>
					</div>
				</article>
			</div>
		<?php endforeach; ?>
		
		<?php if (isset($_SESSION['user_id'])) : ?>
			<div class="col-md-12">
				<a href="<?= base_url($forum->slug . '/' . $topic->slug . '/reply') ?>" class="btn btn-primary">Responder a este tema</a>
			</div>
		<?php endif; ?>
		
	</div><!-- .row -->
</div><!-- .container -->

<?php //var_dump($forum, $topic, $posts); ?>