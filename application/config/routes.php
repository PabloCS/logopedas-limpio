<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['users'] = 'users/index';
$route['users/(:any)/edit'] = 'users/edit/$1';
$route['users/(:any)/delete'] = 'users/delete/$1';
// $route['users/(:any)'] = 'users/index/$1';
$route['users/login'] = 'users/login';
$route['users/register'] = 'users/register';
$route['users/logout'] = 'users/logout';
$route['users/profile/edit'] = 'users/edit';
$route['users/profile/delete'] = 'users/delete';

$route['create_forum'] = 'forum/create_forum';
$route['topic/create'] = 'forum/create_topic';
$route['(:any)/create_topic'] = 'forum/create_topic/$1';
$route['topic/(:any)/(:any)'] = 'forum/topic/$1/$2';
$route['(:any)/(:any)/reply'] = 'forum/create_post/$1/$2';

$route['paypal/success/(:any)'] = 'paypal/success/$1';

$route['default_controller'] = 'pages/view';
$route['(:any)'] = 'pages/view/$1';
$route['404_override'] = 'errors';
$route['translate_uri_dashes'] = FALSE;