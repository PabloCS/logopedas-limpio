<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="//db.onlinewebfonts.com/c/02a0efa4275f78836dfc89db9e21feea?family=Futura+Book" rel="stylesheet" type="text/css"/>
  <title>Logopedas - En construcción</title>
  <style>
    body {
      background: rgb(0,160,223);
      font-family: "Futura-Book", arial;
      color: #fff;
    }
    
    .container {
      padding: 0 1em;
      text-align: center;
    }

    img {
      margin-top: 2em;
    }

    .text-center {
      text-align: center;
    }

    h1 {
      font-weight: 200;
      font-size: 3em;
    }

    p {
      font-size: 1.8em;
    }

  </style>
</head>
<body>
  <div class="container">
    <img src="https://gitlab.com/PabloCS/logopedas-limpio/-/raw/master/assets/images/png/CPLC_IMAGOTIPO_VERT_BALNCO.png" alt="Logo logopedas blanco">
    <div class="text-center">
        <h1>Página en construcción</h1>
        <p>Nuestra nueva web te dejará sin palabras</p>
    </div>
  </div>
</body>
</html>